# Recomendaciones

## Calidad Visual

La calidad del algoritmo bilineal es mucho superior a el algoritmo del
vecino mas cercano. El algoritmo del vecino mas cercano tiene un
aspecto "pixeleado".

Este pixelado tiene un tamanho similar al bloque por lo que para
imagenes de muy alta resolucion este pixelado no es perceptible.

Ambos algoritmos tienen problemas en regiones con un alto grado de
ruido done comienzan a mostrar colores falsos.

## Tiempo de Procesamiento


| Algoritmo               | Media Procesamiento | Minimo Procesamiento | Maximo Procesamiento |
| ------------------------|:-------------------:|:--------------------:|:--------------------:|
| Vecino Mas Cercano      | 3.8ms               | 2.9ms                |        19.3ms        |
| Bilinear                | 7.2ms               | 5.9ms                |        23.6ms        |

Medido con ~3 minutos en un Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz,
probado en imagenes RGGB 1920x1080.

El tiempo de procesamiento del algoritmo bilinear es aproximadamente
el doble del algoritmo del vecino mas cercano. Pero estos corren a
138Hz y 263Hz por lo que para sistemas con capacidad de ejecutar estos
no deberia de haber problema para ejecutar estos en tiempo real.

## Complejidad Requerida para Implementar el Algoritmo

### Vecino mas Cercano

Este algoritmo se realiza solo con copias de memoria, estoy permite su
realizacion en pocas lineas, estoy reduce su propencidad a posibles
errores y comportamientos no deseados.

### Binlinear

Para los bloques de pixeles que no se encuentran adyacentes a los
bordes su implementacion consiste de unas simples multiplicaciones y
sumas, estas no cambian de un bloque a otro.

La complejidad de este algoritmo viene de aquellos bloques en los
bordes o en las esquinas, esto requiere de logica extra para reducir 

## Paralelismo

En este algoritmo no hay dependencia entre los distintos bloques de
pixeles, por lo que es un buen candidato para procesamiento en GPU o
FPGAs.
