SOURCES=simulator.cpp prototype.cpp statistics.cpp
OBJECTS=$(patsubst %.cpp,%.o,$(SOURCES))
OUTPUT=simulator
DEPENDENCIES=gstreamer-1.0 gstreamer-app-1.0 gstreamer-video-1.0
CFLAGS=-Ofast -Werror -march=native -flto

.PHONY: all clean

all: simulator

%.o : %.cpp
	@g++ $(CFLAGS) -c $< -o $@ `pkg-config --cflags $(DEPENDENCIES)`

$(OUTPUT): $(OBJECTS)
	@g++ $^ -o $@ `pkg-config --libs $(DEPENDENCIES)`

indent:
	@astyle --style=linux  $(SOURCES)

clean:
	@rm -f $(OBJECTS) $(OUTPUT) *~ \#*
