/*
 * Copyright (C) 2020 Michael Gruner <grunermonzon@gmail.com>
 *
 * This file is part of Simulator
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Modified by: Emmanuel Madrigal <emma.madri97@gmail.com>
 *
 */

#include "prototype.h"

#include <string.h>

#define RGB_SIZE 3
#define STEP_SIZE 2

/*
 * RGGB
 * R1 | G1
 * G2 | B1
 *
 * RGB
 * RGB | RGB
 * RGB | RGB
 */
void
nearest_neighbor_process (guint8 * restrict dst, const guint8  * restrict const src,
                          const guint width, const guint height)
{
    guint8 R1, G1, G2, B1;
    guint i, j;

    for(j = 0; j < height; j += STEP_SIZE) {
        for(i = 0; i < width; i += STEP_SIZE) {
            R1 = src[i + j * width];
            G1 = src[(i + 1) + j * width];
            G2 = src[i + (j + 1) * width];
            B1 = src[(i + 1) + (j + 1) * width];

            /* Top Left Pixel */
            dst[(i + j * width) * RGB_SIZE] = R1;
            dst[(i + j * width) * RGB_SIZE + 1] = G1;
            dst[(i + j * width) * RGB_SIZE + 2] = B1;
            /* Top Right Component */
            dst[((i + 1) + j * width) * RGB_SIZE] = R1;
            dst[((i + 1) + j * width) * RGB_SIZE + 1] = G1;
            dst[((i + 1) + j * width) * RGB_SIZE + 2] = B1;
            /* Bottom Left Component */
            dst[(i + (j + 1) * width) * RGB_SIZE] = R1;
            dst[(i + (j + 1) * width) * RGB_SIZE + 1] = G2;
            dst[(i + (j + 1) * width) * RGB_SIZE + 2] = B1;
            /* Bottom Right Component */
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE] = R1;
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE + 1] = G2;
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE + 2] = B1;
        }
    }
}

/*
 * RGGB
 *
 * 2x2 Top Left Block (starts in R1 )
 * R1* | G4 | R2
 * G5  | B4 | G6
 * R3  | G8 | R4
 *
 * 2x2 Top Right Block (starts in R1 )
 * G3 | R1* | G4
 * B3 | G5  | B4
 * G7 | R3  | G8
 *
 * 2x2 Bottom Left Block (starts in R1 )
 * G1  | B2 | G2
 * R1* | G4 | R2
 * G5  | B4 | G6
 *
 * 2x2 Bottom Right Block (starts in R1 )
 * B1 | G1  | B2
 * G3 | R1* | G4
 * B3 | G5  | B4
 *
 * 2x2 Left Blocks (starts in R1 )
 * G1  | B2 | G2
 * R1* | G4 | R2
 * G5  | B4 | G6
 * R3  | G8 | R4
 *
 * 2x2 Top Blocks (starts in R1 )
 * G3 | R1* | G4 | R2
 * B3 | G5  | B4 | G6
 * G7 | R3  | G8 | R4
 *
 * 2x2 Blocks in the center (starts in R1 )
 * B1 | G1  | B2 | G2
 * G3 | R1* | G4 | R2
 * B3 | G5  | B4 | G6
 * G7 | R3  | G8 | R4
 *
 * 2x2 Right Blocks (starts in R1 )
 * B1 | G1  | B2
 * G3 | R1* | G4
 * B3 | G5  | B4
 * G7 | R3  | G8
 *
 * 2x2 Bottom Blocks (starts in R1 )
 * B1 | G1  | B2 | G2
 * G3 | R1* | G4 | R2
 * B3 | G5  | B4 | G6
 *
 * RGB
 * RGB | RGB
 * RGB | RGB
 */
void
bilinear_process (guint8 * restrict dst, const guint8  * restrict const src,
                  const guint width, const guint height)
{
    guint8 R1, R2, R3, R4;
    guint8 B1, B2, B3, B4;
    guint8 G1, G3, G4, G5, G6, G8;
    guint j = 0, i = 0;

    /* Top Left Corner */
    {
        /* i=0, j=0 */
        R1 = src[0];
        R2 = src[2];
        R3 = src[2 * width];
        R4 = src[2 + 2 * width];

        G4 = src[1];
        G5 = src[width];
        G6 = src[2 + width];
        G8 = src[1 + 2 *  width];

        B4 = src[1 + width];

        /* Top Left Pixel */
        dst[0] = R1;
        dst[1] = (G4 + G5) / 2;
        dst[2] = B4;
        /* Top Right Component */
        dst[RGB_SIZE] = (R1 + R2) / 2;
        dst[RGB_SIZE + 1] = G4;
        dst[RGB_SIZE + 2] = B4;
        /* Bottom Left Component */
        dst[width * RGB_SIZE] = (R1 + R3) / 2;
        dst[width * RGB_SIZE + 1] = G5;
        dst[width * RGB_SIZE + 2] = B4;
        /* Bottom Right Component */
        dst[(1 + width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[(1 + width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[(1 + width) * RGB_SIZE + 2] = B4;
    }

    /* Top Border */
    for(i = STEP_SIZE; i < width - STEP_SIZE; i += STEP_SIZE) {
        /* j=0 */
        R1 = src[i];
        R2 = src[(i + 2)];
        R3 = src[i + 2 * width];
        R4 = src[(i + 2) + 2 * width];

        G3 = src[(i - 1)];
        G4 = src[(i + 1)];
        G5 = src[i + width];
        G6 = src[(i + 2) + width];
        G8 = src[(i + 1) + 2 *  width];

        B3 = src[(i - 1) + width];
        B4 = src[(i + 1) + width];

        /* Top Left Pixel */
        dst[i * RGB_SIZE] = R1;
        dst[i * RGB_SIZE + 1] = (G3 + G4 + G5) / 3;
        dst[i * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Top Right Component */
        dst[(i + 1) * RGB_SIZE] = (R1 + R2) / 2;
        dst[(i + 1) * RGB_SIZE + 1] = G4;
        dst[(i + 1) * RGB_SIZE + 2] = B4;
        /* Bottom Left Component */
        dst[(i +  width) * RGB_SIZE] = (R1 + R3) / 2;
        dst[(i +  width) * RGB_SIZE + 1] = G5;
        dst[(i +  width) * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Bottom Right Component */
        dst[((i + 1) +  width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[((i + 1) +  width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[((i + 1) +  width) * RGB_SIZE + 2] = B4;
    }

    /* Top Right Corner */
    {
        /* i=width - STEP_SIZE, 0 = 0 */
        R1 = src[width - STEP_SIZE];
        R2 = src[width - STEP_SIZE + 2];
        R3 = src[3 * width - STEP_SIZE];
        R4 = src[3 * width - STEP_SIZE + 2];

        G3 = src[width - STEP_SIZE - 1];
        G4 = src[width - STEP_SIZE + 1];
        G5 = src[width - STEP_SIZE + width];
        G6 = src[2 * width - STEP_SIZE + 2];
        G8 = src[3 * width - STEP_SIZE + 1];

        B3 = src[2 * width - STEP_SIZE - 1];
        B4 = src[2 * width - STEP_SIZE + 1];

        /* Top Left Pixel */
        dst[(width - STEP_SIZE) * RGB_SIZE] = R1;
        dst[(width - STEP_SIZE) * RGB_SIZE + 1] = (G3 + G4 + G5) / 3;
        dst[(width - STEP_SIZE) * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Top Right Component */
        dst[(width - STEP_SIZE + 1) * RGB_SIZE] = (R1 + R2) / 2;
        dst[(width - STEP_SIZE + 1) * RGB_SIZE + 1] = G4;
        dst[(width - STEP_SIZE + 1) * RGB_SIZE + 2] = B4;
        /* Bottom Left Component */
        dst[(2 * width - STEP_SIZE) * RGB_SIZE] = (R1 + R3) / 2;
        dst[(2 * width - STEP_SIZE) * RGB_SIZE + 1] = G5;
        dst[(2 * width - STEP_SIZE) * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Bottom Rwidth - STEP_SIZEght Component */
        dst[(2 * width - STEP_SIZE + 1) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[(2 * width - STEP_SIZE + 1) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[(2 * width - STEP_SIZE + 1) * RGB_SIZE + 2] = B4;
    }


    /* Process all 2x2 blocks */
    for(j = STEP_SIZE; j < height - STEP_SIZE; j += STEP_SIZE) {

        /* Left Border */
        {
            /* i = 0*/
            R1 = src[j * width];
            R2 = src[2 + j * width];
            R3 = src[(j + 2) * width];
            R4 = src[2 + (j + 2) * width];

            G1 = src[(j - 1) * width];
            G4 = src[1 + j * width];
            G5 = src[(j + 1) * width];
            G6 = src[2 + (j + 1) * width];
            G8 = src[1 + (j + 2) * width];

            B2 = src[1 + (j - 1) * width];
            B4 = src[(j + 1) * width];

            /* Top Left Pixel */
            dst[(j * width) * RGB_SIZE] = R1;
            dst[(j * width) * RGB_SIZE + 1] = (G1 + G4 + G5) / 3;
            dst[(j * width) * RGB_SIZE + 2] = (B2 + B4) / 2;

            /* Top Right Component */
            dst[(1 + j * width) * RGB_SIZE] = (R1 + R2) / 2;
            dst[(1 + j * width) * RGB_SIZE + 1] = G4;
            dst[(1 + j * width) * RGB_SIZE + 2] = (B2 + B4) / 2;

            /* Bottom Left Component */
            dst[((j + 1) * width) * RGB_SIZE] = (R1 + R3) / 2;
            dst[((j + 1) * width) * RGB_SIZE + 1] = G5;
            dst[((j + 1) * width) * RGB_SIZE + 2] = B4;

            /* Bottom Right Component */
            dst[(1 + (j + 1) * width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
            dst[(1 + (j + 1) * width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
            dst[(1 + (j + 1) * width) * RGB_SIZE + 2] = B4;
        }

        for(i = STEP_SIZE; i < width - STEP_SIZE; i += STEP_SIZE) {
            R1 = src[i + j * width];
            R2 = src[(i + 2) + j * width];
            R3 = src[i + (j + 2) * width];
            R4 = src[(i + 2) + (j + 2) * width];

            G1 = src[i + (j - 1) * width];
            G3 = src[(i - 1) + j * width];
            G4 = src[(i + 1) + j * width];
            G5 = src[i + (j + 1) * width];
            G6 = src[(i + 2) + (j + 1) * width];
            G8 = src[(i + 1) + (j + 2) * width];

            B1 = src[(i - 1) + (j - 1) * width];
            B2 = src[(i + 1) + (j - 1) * width];
            B3 = src[(i - 1) + (j + 1) * width];
            B4 = src[(i + 1) + (j + 1) * width];

            /* Top Left Pixel */
            dst[(i + j * width) * RGB_SIZE] = R1;
            dst[(i + j * width) * RGB_SIZE + 1] = (G1 + G3 + G4 + G5) / 4;
            dst[(i + j * width) * RGB_SIZE + 2] = (B1 + B2 + B3 + B4) / 4;
            /* Top Right Component */
            dst[((i + 1) + j * width) * RGB_SIZE] = (R1 + R2) / 2;
            dst[((i + 1) + j * width) * RGB_SIZE + 1] = G4;
            dst[((i + 1) + j * width) * RGB_SIZE + 2] = (B2 + B4) / 2;
            /* Bottom Left Component */
            dst[(i + (j + 1) * width) * RGB_SIZE] = (R1 + R3) / 2;
            dst[(i + (j + 1) * width) * RGB_SIZE + 1] = G5;
            dst[(i + (j + 1) * width) * RGB_SIZE + 2] = (B3 + B4) / 2;
            /* Bottom Right Component */
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
            dst[((i + 1) + (j + 1) * width) * RGB_SIZE + 2] = B4;
        }

        /* Right Border */
        {
            /* i = width - STEP_SIZE */
            R1 = src[-STEP_SIZE + (j + 1) * width];
            R3 = src[-STEP_SIZE + (j + 3) * width];

            G1 = src[-STEP_SIZE + j * width];
            G4 = src[(1 - STEP_SIZE) + (j + 1) * width];
            G5 = src[-STEP_SIZE + (j + 2) * width];
            G8 = src[(1 - STEP_SIZE) + (j + 3) * width];

            B2 = src[(1 - STEP_SIZE) + j  * width];
            B4 = src[(1 - STEP_SIZE) + (j + 2) * width];

            /* Top Left Pixel */
            dst[(-STEP_SIZE + (j + 1) * width) * RGB_SIZE] = R1;
            dst[(-STEP_SIZE + (j + 1) * width) * RGB_SIZE + 1] = (G1 + G3 + G4 + G5) / 4;
            dst[(-STEP_SIZE + (j + 1) * width) * RGB_SIZE + 2] = (B1 + B2 + B3 + B4) / 4;

            /* Top Right Component */
            dst[((1 - STEP_SIZE) + (j + 1) * width) * RGB_SIZE] = R1;
            dst[((1 - STEP_SIZE) + (j + 1) * width) * RGB_SIZE + 1] = G4;
            dst[((1 - STEP_SIZE) + (j + 1) * width) * RGB_SIZE + 2] = (B2 + B4) / 2;

            /* Bottom Left Component */
            dst[(-STEP_SIZE + (j + 2) * width) * RGB_SIZE] = (R1 + R3) / 2;
            dst[(-STEP_SIZE + (j + 2) * width) * RGB_SIZE + 1] = G5;
            dst[(-STEP_SIZE + (j + 2) * width) * RGB_SIZE + 2] = (B3 + B4) / 2;

            /* Bottom Right Component */
            dst[((1 - STEP_SIZE) + (j + 2) * width) * RGB_SIZE] = (R1 + R3) / 2;
            dst[((1 - STEP_SIZE) + (j + 2) * width) * RGB_SIZE + 1] = (G4 + G5 + G8) / 3;
            dst[((1 - STEP_SIZE) + (j + 2) * width) * RGB_SIZE + 2] = B4;
        }
    }

    /* Bottom Left Corner */
    {
        /* i = 0; j = height - STEP_SIZE */
        R1 = src[(height - STEP_SIZE) * width];
        R2 = src[2 + (height - STEP_SIZE) * width];

        G1 = src[(height - STEP_SIZE - 1) * width];
        G4 = src[1 + (height - STEP_SIZE) * width];
        G5 = src[(height - STEP_SIZE + 1) * width];
        G6 = src[2 + (height - STEP_SIZE + 1) * width];

        B2 = src[1 + (height - STEP_SIZE - 1) * width];
        B4 = src[1 + (height - STEP_SIZE + 1) * width];

        /* Top Left Pixel */
        dst[(0 + (height - STEP_SIZE) * width) * RGB_SIZE] = R1;
        dst[(0 + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = (G1 + G4 + G5) / 3;
        dst[(0 + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B2 + B4) / 2;
        /* Top Right Component */
        dst[((0 + 1) + (height - STEP_SIZE) * width) * RGB_SIZE] = (R1 + R2) / 2;
        dst[((0 + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = G4;
        dst[((0 + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B2 + B4) / 2;
        /* Bottom Left Component */
        dst[(0 + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R3) / 2;
        dst[(0 + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = G5;
        dst[(0 + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = B4;
        /* Bottom Right Component */
        dst[((0 + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[((0 + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[((0 + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = B4;
    }

    /* Top Border */
    for(i = STEP_SIZE; i < width - STEP_SIZE; i += STEP_SIZE) {
        /* j = height - STEP_SIZE */
        R1 = src[i + (height - STEP_SIZE) * width];
        R2 = src[(i + 2) + (height - STEP_SIZE) * width];

        G1 = src[i + (height - STEP_SIZE - 1) * width];
        G3 = src[(i - 1) + (height - STEP_SIZE) * width];
        G4 = src[(i + 1) + (height - STEP_SIZE) * width];
        G5 = src[i + (height - STEP_SIZE + 1) * width];
        G6 = src[(i + 2) + (height - STEP_SIZE + 1) * width];

        B1 = src[(i - 1) + (height - STEP_SIZE - 1) * width];
        B2 = src[(i + 1) + (height - STEP_SIZE - 1) * width];
        B3 = src[(i - 1) + (height - STEP_SIZE + 1) * width];
        B4 = src[(i + 1) + (height - STEP_SIZE + 1) * width];

        /* Top Left Pixel */
        dst[(i + (height - STEP_SIZE) * width) * RGB_SIZE] = R1;
        dst[(i + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = (G1 + G3 + G4 + G5) / 4;
        dst[(i + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B1 + B2 + B3 + B4) / 4;
        /* Top Right Component */
        dst[((i + 1) + (height - STEP_SIZE) * width) * RGB_SIZE] = (R1 + R2) / 2;
        dst[((i + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = G4;
        dst[((i + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B2 + B4) / 2;
        /* Bottom Left Component */
        dst[(i + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R3) / 2;
        dst[(i + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = G5;
        dst[(i + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Bottom Right Component */
        dst[((i + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[((i + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[((i + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = B4;
    }

    /* Bottom Right Corner */
    {
        /* i = width - STEP_SIZE; j = height - STEP_SIZE */
        R1 = src[width - STEP_SIZE + (height - STEP_SIZE) * width];
        R2 = src[(width - STEP_SIZE + 2) + (height - STEP_SIZE) * width];

        G1 = src[width - STEP_SIZE + (height - STEP_SIZE - 1) * width];
        G3 = src[(width - STEP_SIZE - 1) + (height - STEP_SIZE) * width];
        G4 = src[(width - STEP_SIZE + 1) + (height - STEP_SIZE) * width];
        G5 = src[width - STEP_SIZE + (height - STEP_SIZE + 1) * width];
        G6 = src[(width - STEP_SIZE + 2) + (height - STEP_SIZE + 1) * width];

        B1 = src[(width - STEP_SIZE - 1) + (height - STEP_SIZE - 1) * width];
        B2 = src[(width - STEP_SIZE + 1) + (height - STEP_SIZE - 1) * width];
        B3 = src[(width - STEP_SIZE - 1) + (height - STEP_SIZE + 1) * width];
        B4 = src[(width - STEP_SIZE + 1) + (height - STEP_SIZE + 1) * width];

        /* Top Left Pixel */
        dst[(width - STEP_SIZE + (height - STEP_SIZE) * width) * RGB_SIZE] = R1;
        dst[(width - STEP_SIZE + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = (G1 + G3 + G4 + G5) / 4;
        dst[(width - STEP_SIZE + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B1 + B2 + B3 + B4) / 4;
        /* Top Right Component */
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE) * width) * RGB_SIZE] = (R1 + R2) / 2;
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 1] = G4;
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE) * width) * RGB_SIZE + 2] = (B2 + B4) / 2;
        /* Bottom Left Component */
        dst[(width - STEP_SIZE + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R3) / 2;
        dst[(width - STEP_SIZE + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = G5;
        dst[(width - STEP_SIZE + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = (B3 + B4) / 2;
        /* Bottom Right Component */
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE] = (R1 + R2 + R3 + R4) / 4;
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 1] = (G4 + G5 + G6 + G8) / 4;
        dst[((width - STEP_SIZE + 1) + (height - STEP_SIZE + 1) * width) * RGB_SIZE + 2] = B4;
    }


}
